# nodejs-linenumber

Get line number in node.js module or script for debugging or other use.
*  Usage:
*  var ln = require('nodejs-linenumber');
*  console.log('Happening at line: ' + ln());